<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>CB Admin | Dashboard</title>
     <?php echo
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    header('Content-Type: text/html');?>
    
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
      {!!Html::script('assets/js/scroller.jpgiclo.js')!!}
      {!!Html::script('assets/js/bootstrap.min.js')!!}
      {!!Html::script('assets/js/app.js')!!}

      {!!Html::style('assets/css/bootstrap.min.css')!!}
      {!!Html::style('assets/css/cbstyle.css')!!}
      {!!Html::style('assets/css/cbskins.css')!!}
      {!!Html::style('assets/css/font-awesome.min.css')!!}
  </head>