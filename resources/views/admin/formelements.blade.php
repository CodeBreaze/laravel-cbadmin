@include('admin.includes.common-header')
@include('admin.includes.top-left-nav')
      <div class="content-wrapper">
        <section class="content">
          <div class="row">
            <div class="col-xs-8 centrfrm">
            <section class="content-header">
          <h1>Form elements</h1>
          <span><a href="#">Add new</a></span>
         </section>
              <div class="box">
                <div class="box-body">
                 <div class="form-group">
                      <label for="exampleInputEmail1">Email address</label>
                      <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Password</label>
                      <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputFile">File input</label>
                      <input class="form-control" type="file" id="exampleInputFile">
                      <p class="help-block">Example block-level help text here.</p>
                    </div>
                    <div class="checkbox">
                      <label>
                        <input type="checkbox"> Check me out
                      </label>
                    </div>
                    <div>
                      <select class="form-control">
                        <option value="2">Option 1</option>
                        <option value="2">Option 1</option>
                        <option value="2">Option 1</option>
                        <option value="2">Option 1</option>
                      </select>
                    </div>
                    <input type="submit" class="btn btn-primary"/>
                    <input type="submit" value="Cancel" class="btn btn-primary"/>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      @include('admin.includes.common-footer')