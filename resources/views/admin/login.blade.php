<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>CBAdmin v1.0 | Login</title>
     <?php echo
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    header('Content-Type: text/html');?>
    
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

      {!!Html::style('assets/css/bootstrap.min.css')!!}
      {!!Html::style('assets/css/AdminLTE.css')!!}
      {!!Html::style('assets/css/_all-skins.min.css')!!}
      {!!Html::style('assets/css/font-awesome.min.css')!!}
  </head>
  <body class="login-page">
    <div class="login-box">
      <div class="login-logo">
        <b>CB</b>Admin
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        @if(Session::has('errorlogin'))<p class="login-box-msg">{{Session::get('errorlogin')}}</p>@endif
        {!!Form::open(['route'=>'do_login'])!!}
          <div class="form-group has-feedback">
            <input type="text" class="form-control" name="username" value="cbadmin" required placeholder="Username"/>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" name="password" value="admin!@#123" required placeholder="Password"/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div><!-- /.col -->
          </div>
       {!!Form::close()!!}
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
  </body>
</html>