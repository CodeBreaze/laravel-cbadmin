@include('admin.includes.common-header')
@include('admin.includes.top-left-nav')
      <div class="content-wrapper">
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
            <section class="content-header">
          <h1>Tables</h1>
          <span><a href="#">Add new</a></span>
         </section>
              <div class="box">
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Rendering engine</th>
                        <th>Browser</th>
                        <th>Platform(s)</th>
                        <th>Engine version</th>
                        <th>CSS grade</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Trident</td>
                        <td>Internet
                          Explorer 4.0</td>
                        <td>Win 95+</td>
                        <td> 4</td>
                        <td>X</td>
                        <td>
                          <span><a class="editThis" href="#"></a></span>
                          <span><a onclick="return confirm('Are you sure you want to delete this item?');" class="dltThis" href="#"></a></span>
                        </td>
                      </tr><tr>
                        <td>Trident</td>
                        <td>Internet
                          Explorer 4.0</td>
                        <td>Win 95+</td>
                        <td> 4</td>
                        <td>X</td>
                        <td>
                          <span><a class="editThis" href="#"></a></span>
                          <span><a onclick="return confirm('Are you sure you want to delete this item?');" class="dltThis" href="#"></a></span>
                        </td>
                      </tr><tr>
                        <td>Trident</td>
                        <td>Internet
                          Explorer 4.0</td>
                        <td>Win 95+</td>
                        <td> 4</td>
                        <td>X</td>
                        <td>
                          <span><a class="editThis" href="#"></a></span>
                          <span><a onclick="return confirm('Are you sure you want to delete this item?');" class="dltThis" href="#"></a></span>
                        </td>
                      </tr><tr>
                        <td>Trident</td>
                        <td>Internet
                          Explorer 4.0</td>
                        <td>Win 95+</td>
                        <td> 4</td>
                        <td>X</td>
                        <td>
                          <span><a class="editThis" href="#"></a></span>
                          <span><a onclick="return confirm('Are you sure you want to delete this item?');" class="dltThis" href="#"></a></span>
                        </td>
                      </tr><tr>
                        <td>Trident</td>
                        <td>Internet
                          Explorer 4.0</td>
                        <td>Win 95+</td>
                        <td> 4</td>
                        <td>X</td>
                        <td>
                          <span><a class="editThis" href="#"></a></span>
                          <span><a onclick="return confirm('Are you sure you want to delete this item?');" class="dltThis" href="#"></a></span>
                        </td>
                      </tr><tr>
                        <td>Trident</td>
                        <td>Internet
                          Explorer 4.0</td>
                        <td>Win 95+</td>
                        <td> 4</td>
                        <td>X</td>
                        <td>
                          <span><a class="editThis" href="#"></a></span>
                          <span><a onclick="return confirm('Are you sure you want to delete this item?');" class="dltThis" href="#"></a></span>
                        </td>
                      </tr><tr>
                        <td>Trident</td>
                        <td>Internet
                          Explorer 4.0</td>
                        <td>Win 95+</td>
                        <td> 4</td>
                        <td>X</td>
                        <td>
                          <span><a class="editThis" href="#"></a></span>
                          <span><a onclick="return confirm('Are you sure you want to delete this item?');" class="dltThis" href="#"></a></span>
                        </td>
                      </tr><tr>
                        <td>Trident</td>
                        <td>Internet
                          Explorer 4.0</td>
                        <td>Win 95+</td>
                        <td> 4</td>
                        <td>X</td>
                        <td>
                          <span><a class="editThis" href="#"></a></span>
                          <span><a onclick="return confirm('Are you sure you want to delete this item?');" class="dltThis" href="#"></a></span>
                        </td>
                      </tr>
                     
                    </tbody>
                   
                  </table>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
      @include('admin.includes.common-footer')