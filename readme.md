# ** CB Admin Laravel 5.1 with Bootstrap ** 

CB Admin with Bootstrap is a developement envirment with Laravel 5.1

### Features

	* Bootstrap 3.x Supported
	* Laravel Authentication & Middleware integrated
	* Login, Dashboard, Tables, Form Elements Pages

### Packages included

	* Guzzle HTTP library
	* Form & HTML class
	* Login, Dashboard, Tables, Form Elements

### Requirements

	* PHP >= 5.5.9
	* OpenSSL PHP Extension
	* PDO PHP Extension
	* Mbstring PHP Extension
	* Tokenizer PHP Extension
	* Composer

## **Installation** ##
	
####Step 1: BitBucket Clone

	git clone git@bitbucket.org:CodeBreaze/laravel-cbadmin.git

####Step 2: Make sure app/storage is writable by your web server.

	chmod -R 777 <folder_name>

####Step 3:  Use Composer to install dependencies

	Point to project directory ( Cloned Directory ) with Terminal/Command Prompt.

	composer Install

####Step 3: Configure Environments

	Change Database Connection in .env file from the root directory.

####Step 4: Populate Database

	php artisan migrate:refresh --seed

####Step 5: Set Encryption Key

	php artisan key:generate --env=local

## **Navigation** ##

####Admin Login

	  /superadmin/login

		username : cbadmin
		password : admin!@#123

### License
 
This is free software distributed under the terms of the MIT license

### Issues

Any Queries..[Ping Me!](mailto:info@codebreaze.com).