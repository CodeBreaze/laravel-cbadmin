<?php

/*SUPER ADMIN */
Route::group(['prefix'=>'superadmin'],function(){
	Route::get('login',['as'=>'superadmnlogin','uses'=>'SuperAdminController@login']);
	Route::post('do-login',['as'=>'do_login','uses'=>'SuperAdminController@DoLogin']);
	Route::get('do-logout',['as'=>'logout','uses'=>'SuperAdminController@logout']);

	Route::group(['middleware'=>'auth'],function(){
		Route::get('dashboard',['as'=>'dashboard','uses'=>'SuperAdminController@dashboard']);
		Route::get('form-elements',['as'=>'form_elements','uses'=>'SuperAdminController@FormElements']);
		Route::get('tables',['as'=>'tables','uses'=>'SuperAdminController@Tables']);
	});
	
});
/*SUPER ADMIN */

/*FROND END*/
Route::get('/',['as'=>'frondend','uses'=>'IndexController@index']);
/*FROND END*/

