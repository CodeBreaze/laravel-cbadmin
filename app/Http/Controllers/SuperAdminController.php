<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;use Input;use Auth;use Redirect;

class SuperAdminController extends Controller
{
    public function login(){
    	return View::make('admin.login');
    }
    public function DoLogin(){
    	$userdata = [
    		'username'=>Input::get('username'),
    		'password'=>Input::get('password'),
    	];
    	if(Auth::attempt($userdata)){
    		return Redirect::to(route('dashboard'));
    	}else{
    		return Redirect::to(route('superadmnlogin'))->with('errorlogin','Invalid username or password.');
    	}
    }
    public function logout(){
        Auth::logout();
        return Redirect::to(route('superadmnlogin'))->with('errorlogin','Succesfully Logged Out !!.');
    }
    public function dashboard(){
    	return View::make('admin.dashboard');
    }
    public function FormElements(){
        return View::make('admin.formelements');
    }
    public function Tables(){
        return View::make('admin.tables');
    }
}
