<?php

use Illuminate\Database\Seeder;

class UserTableSeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

        $userarray = [[
        	'username'=>'cbadmin',
        	'email'=>'info@cbadmin.com',
        	'password'=>Hash::make('admin!@#123'),
        	'created_at'=>date('Y-m-d H:i:s')
        ]];

        DB::table('users')->insert($userarray);
    }
}
